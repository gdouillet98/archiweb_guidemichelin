<?php
namespace App\Controller;

use App\Entity\Restaurant;
use App\Form\Type\RestaurantType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class guideController extends AbstractController
{
    public function accueil()
    {
        return $this->render('index.html.twig');
    }

    public function afficher_menu()
    {
        return $this->render('menu.html.twig');
    }

    public function voir($id)
    {
        $resto = $this->getDoctrine()->getRepository(Restaurant::class)->find($id);
        if (!$resto) {
            throw $this->createNotFoundException('Resto[id=' . $id . '] inexistante');
        }

        return $this->render('voir.html.twig',
            array('resto' => $resto));
    }

    public function ajouter($nom, $chef, $nb_etoile)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $resto = new Restaurant;
        $resto->setNom($nom);
        $resto->setChef($chef);
        $resto->setNbEtoile($nb_etoile);
        $entityManager->persist($resto);
        $entityManager->flush();
        return $this->redirectToRoute('voir',
            array('id' => $resto->getId()));
    }

    public function ajouter2(Request $request)
    {
        $resto = new Restaurant;
        $form = $this->createForm(RestaurantType::class, $resto, ['action' => $this->generateUrl('formulaire')]);
        $form->add('submit', SubmitType::class, array('label' => 'Ajouter'));
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($resto);
            $entityManager->flush();
            return $this->redirectToRoute('voir', array('id' => $resto->getId()));
        }
        return $this->render('ajouter2.html.twig', array('monFormulaire' => $form->createView()));
    }

    function list() {
        $listresto = $this->getDoctrine()
            ->getRepository(Restaurant::class)->findAll();
        return $this->render('list.html.twig',
            array('listresto' => $listresto));
    }

    public function listchef()
    {
        $listresto = $this->getDoctrine()
            ->getRepository(Restaurant::class)->findAll();
        return $this->render('listchef.html.twig',
            array('listresto' => $listresto));
    }

    public function modifier($id)
    {
        $resto = $this->getDoctrine()->getRepository(Restaurant::class)->find($id);
        if (!$resto) {
            throw $this->createNotFoundException('Resto[id=' . $id . '] inexistante');
        }

        $form = $this->createForm(RestaurantType::class, $resto,
            ['action' => $this->generateUrl('resto_modifier_suite',
                array('id' => $resto->getId()))]);
        $form->add('submit', SubmitType::class, array('label' => 'Modifier'));
        return $this->render('modifier.html.twig',
            array('monFormulaire' => $form->createView()));
    }
    public function modifierSuite(Request $request, $id)
    {
        $resto = $this->getDoctrine()->getRepository(Restaurant::class)->find($id);
        if (!$resto) {
            throw $this->createNotFoundException('Resto[id=' . $id . '] inexistante');
        }

        $form = $this->createForm(RestaurantType::class, $resto,
            ['action' => $this->generateUrl('resto_modifier_suite',
                array('id' => $resto->getId()))]);
        $form->add('submit', SubmitType::class, array('label' => 'Modifier'));
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($resto);
            $entityManager->flush();
            $url = $this->generateUrl('voir',
                array('id' => $resto->getId()));
            return $this->redirect($url);
        }
        return $this->render('modifier.html.twig',
            array('monFormulaire' => $form->createView()));
    }

    public function trietoile($nb) {
        $repo = $this->getDoctrine()->getManager()->getRepository(Restaurant::class);
        $salles = $repo->findBy(array('nb_etoile'=>$nb));
        return $this->render('trietoile.html.twig',
            array('salles' => $salles));
    }

    public function trichef($nom) {
        $repo = $this->getDoctrine()->getManager()->getRepository(Restaurant::class);
        $salles = $repo->findBy(array('chef'=>$nom));
        return $this->render('trichef.html.twig',
            array('salles' => $salles));
    }
}
