<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RestaurantRepository")
* @ORM\HasLifecycleCallbacks()
 */
class Restaurant
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;
    

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $nom;

    /**
 * @ORM\PrePersist
 * @ORM\PreUpdate
 */
    public function corrigeNom() {
    $this->nom = strtoupper($this->nom);
    } 

    /**
     * @ORM\Column(type="string", length=25)
     */
    private $chef;

    /**
     * @ORM\Column(type="smallint")
     * @Assert\Regex(pattern="/^[1-3]$/",
     * message="la valeur doit être comprise est comprise entre 1 et 3.")
     */
    private $nb_etoile;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getChef(): ?string
    {
        return $this->chef;
    }

    public function setChef(string $chef): self
    {
        $this->chef = $chef;

        return $this;
    }

    public function getNbEtoile(): ?int
    {
        return $this->nb_etoile;
    }

    public function setNbEtoile(int $nb_etoile): self
    {
        $this->nb_etoile = $nb_etoile;

        return $this;
    }
}
